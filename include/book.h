#ifndef BOOK_FUNCTIONS
#define BOOK_FUNCTIONS
#include "structs.h"

void request_book_details(collection *book);
int add_book(collection *book, char title[], char author[]);
int remove_book(collection *book, int select);
int borrow_book(collection *book, user *student, int select, int stu);
int return_book(collection *book, int select);
void list_all_borrowed(collection *book, user *student, int stu);
void list_all_books(collection *book);
void search_books(collection *book, char search[]);
void list_all_borrowedd(collection *book);
void search_borrowed(collection *book, char search[]);

#endif
*
