#ifndef STRUCT_TYPES
#define STRUCT_TYPES

//All struct definitions.

#define max_name 30
#define max_ID 10
#define max_course 20
#define max_email 50
#define max_password 20
#define max_title 30
#define max_author 30
#define max_date_published 10

typedef struct {
  char name[max_name];
  char ID[max_ID];
  char email[max_email];
  char password[max_password];
} user;

typedef struct {
  char email[max_email];
  char password[max_password];
} admin;

typedef struct {
  char title[max_title];
  char author[max_author];
  char borrowerID[max_ID];
} collection;

#endif
