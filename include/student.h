#ifndef STUDENT_FUNCTIONS
#define STUDENT_FUNCTIONS
#include "structs.h"

int student_register_application();
int add_student(char name[], char ID[], char email[], char password[]);
int remove_student(user* student, int select);
void list_all_students(user *student);
void search_students(user *student, char search[]);

#endif
