#ifndef LIBRARIAN_FUNCTIONS
#define LIBRARIAN_FUNCTIONS
#include "structs.h"

void request_librarian_info();
void create_librarian(char email[], char password[]);

#endif
