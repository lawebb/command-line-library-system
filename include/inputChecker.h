#ifndef INPUTCHECK_FUNCTIONS
#define INPUTCHECKS_FUNCTIONS
#include "structs.h"

void ask_name_input();
int null_trial();
int check_name_input(char name[]);
void ask_email_input();
int check_email_input(char email[]);
void ask_password_input();
int check_password_input(char password[]);
void ask_studentID_input();
int check_studentID_input(char ID[]);
void ask_title_input();
int check_title_input(char title[]);
void ask_author_input();
int check_author_input(char author[]);

#endif
