#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>

#include "structs.h"

//Module to handle all functions involving student data.

#define max_name 30
#define max_ID 10
#define max_course 20
#define max_email 50
#define max_password 20
#define max_title 30
#define max_author 30
#define max_date_published 10


extern char name[], ID[], email[], password[];

extern int n, maxSize, select, nav;

//Function to request and check user input for student data.
void student_register_application(user *student) {
  ask_name_input();

  ask_ID_input();

  ask_email_input();

  ask_password_input();

  add_student(student, name, ID, email, password);

  nav = 0;
}


//Function to add the data to the student variable. This will rearrange the structs
//in alphabetical order.
int add_student(user *student, char name[], char ID[], char email[], char password[]) {

  int comp = strcmp(student[0].name, "");
  if(comp == 0) {
    strcpy(student[0].name, name);
    strcpy(student[0].ID, ID);
    strcpy(student[0].email, email);
    strcpy(student[0].password, password);

    n += 1;

    return 0;
  }

  strcpy(student[n].name, name);
  strcpy(student[n].ID, ID);
  strcpy(student[n].email, email);
  strcpy(student[n].password, password);

  n += 1;

  user temp;

  for(int i=0; i<n -1; ++i) {
    for(int j=i+1; j<n; ++j) {
      if(strcmp(student[i].name,student[j].name) > 0){
        temp = student[i];
        student[i] = student[j];
        student[j] = temp;
      }
    }
  }

  return 0;
}


//Function to remove a student. This will rearrange students to fill gaps
//and memset/NULL the last element.
int remove_student(user *student,  int select) {

  user temp;

  for(int i=select, j=select+1; i<n; i++,j++) {
    student[i] = student[j];
  }

  memset(student[n-1].name, 0, sizeof(student[n-1].name));
  memset(student[n-1].ID, 0, sizeof(student[n-1].ID));
  memset(student[n-1].email, 0, sizeof(student[n-1].email));
  memset(student[n-1].password, 0, sizeof(student[n-1].password));

  n -= 1;

  nav = 22;

  return 1;
}


//Function to list all students in table format.
void list_all_students(user *student) {
  int namespace = max_name-4, idspace = max_ID-10;

  printf("\n\n");
  printf("   NAME");
  for(int i=0; i<=namespace; i++) {
    printf(" ");
  }
  printf("|");
  printf("STUDENT ID");
  for(int i=0; i<=idspace; i++) {
    printf(" ");
  }

  for(int j=0; j < n; j++) {
    int namespace = max_name-strlen(student[j].name), idspace = max_ID-strlen(student[j].ID);
    printf("\n%i. %s", j, student[j].name);
    for(int i=0; i<=namespace; i++) {
      printf(" ");
    }
    printf("|");
    printf("%s", student[j].ID);
    for(int i=0; i<=idspace; i++) {
      printf(" ");
    }
  }
  printf("\n\n");

  nav = 22;
}


//Function to search students using a string input and list results
//in table format.
void search_students(user *student, char search[]) {

  int namespace = max_name-4, idspace = max_ID-10;
  printf("\n\n");

  printf("   NAME");
  for(int i=0; i<=namespace; i++) {
    printf(" ");
  }
  printf("|");
  printf("STUDENT ID");
  for(int i=0; i<=idspace; i++) {
    printf(" ");
  }

  for(int k=0; k<n; k++){

    char pch = strstr(student[k].ID, search);
    char pch2 = strstr(student[k].name, search);

    if(pch != NULL || pch2 != NULL) {
      int j = k;

      int namespace = max_name-strlen(student[j].name), idspace = max_ID-strlen(student[j].ID);
      printf("\n%i. %s", j, student[j].name);
      for(int i=0; i<=namespace; i++) {
        printf(" ");
      }
      printf("|");
      printf("%s", student[j].ID);
      for(int i=0; i<=idspace; i++) {
        printf(" ");
      }
    }
  }
  printf("\n\n");

  nav = 22;
}
