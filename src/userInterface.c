#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "structs.h"

//Module to handle the user interface/navigation.

extern int ad, stu, nav, ans;

extern char trial[100];


//Function display options on startup of the software. Offers option to login,
//create a student account, or exit the program entirely.
int opening_interface() {
  printf("\nWelcome to Libration!\n");
  printf("Enter the key of the option you would like to select:\n");
  printf("1. Log-in\n2. Register\n3. Exit\n\n");

  scanf("%i", &ans);

  scanf("%c", &trial);

  printf("\n");

  if(ans == 1) {
    nav = 11;
  }

   else if(ans == 2) {
    nav = 12;
  }

  else if(ans == 3) {
    nav = 100;
  }

  return 1;
}


//Function to display the user interface for the librarian ONLY. Offers librarian
//specific options and returns a navigation path.
void librarian_interface_one() {
  printf("\nEnter the key of the option you would like to select:\n");
  printf("0. Logout\n1. List all books\n2. Search books\n3. Add a book\n4. Remove a book\n");
  printf("5. List all borrowed books\n6. Search borrowed books\n");
  printf("7. List all students\n8. Search students\n9. Remove student\n\n");

  scanf("%i", &ans);

  if(ans == 0) {
    nav = 0;
  }

  else if(ans == 1) {
    nav = 13;
  }

  else if(ans == 2) {
    nav = 14;
  }

  else if(ans == 3) {
    nav = 15;
  }

  else if(ans == 4) {
    nav = 16;
  }

  else if(ans == 5) {
    nav = 24;
  }

  else if(ans == 6) {
    nav = 25;
  }

  else if(ans == 7) {
    nav = 18;
  }

  else if(ans == 8) {
    nav = 19;
  }

  else if(ans == 9) {
    nav = 17;
  }
}


//Function to display the user interface for the student ONLY. Offers student
//specific options and returns a navigation path.
void student_interface_one() {
  printf("\nEnter the key of the option you would like to select:\n");
  printf("0. Logout\n1. List all books\n2. Search books\n3. Borrow a book\n4. Return a book\n\n");

  scanf("%i", &ans);

  if(ans == 0) {
    nav = 0;
  }

  else if(ans == 1) {
    nav = 13;
  }

  else if(ans == 2) {
    nav = 14;
  }

  else if(ans == 3) {
    nav = 20;
  }

  else if(ans == 4) {
    nav = 21;
  }
}
