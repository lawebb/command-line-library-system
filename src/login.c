#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "structs.h"

extern int ad, stu, nav, n;

extern char email[], password[], trial[100];

//Function to test the email and password input of a user to check if correct
//and if so, which account is theirs.
int init_login(user* student, admin librarian) {

  ask_email_input();

  stu = -1, ad = 0;

  int comp = strcmp(librarian.email, email);
  if (comp == 0) {
    ask_password_input();

    int comp2 = strcmp(librarian.password, password);

    if (comp2 == 0) {
      printf("\nYou have successfully logged in. Welcome back Librarian.\n");

      ad = 1;
      stu = -1;

      nav = 22;

      return 1;
    }

    else if(comp2 != 0) {
      printf("\nYou have entered the wrong password. Please select a new option.\n");

      nav=0;

      return 0;
    }
  }

  for(int i=0; i<n; i++) {
    int comp = strcmp(student[i].email, email);

    if (comp == 0) {
      ask_password_input();

      int comp2 = strcmp(student[i].password, password);

      if (comp2 == 0) {
        printf("\nYou have successfully logged in. Welcome back %s.\n", student[i].name);

        ad = 0;
        stu = i;

        nav = 23;

        return 1;
      }
      else if(comp2 != 0) {
        printf("\nYou have entered the wrong password. Please select a new option.\n");

        nav = 0;

        return 0;
      }
    }
  }

  printf("\nThis email is not registered in the system. Please select a new option: ");

  nav = 0;

  return 0;
}
