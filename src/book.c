#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>

#include "structs.h"

//Module to handle all book data.

#define max_name 30
#define max_ID 10
#define max_course 20
#define max_email 50
#define max_password 20
#define max_title 30
#define max_author 30
#define max_date_published 10


extern char title[], author[], trial[];

extern int bn, bmaxSize, select, nav, ad, stu;

//Function to ask for and check book input.
void request_book_details(collection *book) {

  scanf("%c", &trial);

  ask_title_input();

  ask_author_input();

  add_book(book, title, author);
}

//Function to add tested input to a new book variable. And rearrange the structs
//in alphabetical order (based on title).
int add_book(collection *book, char title[], char author[]) {

    nav = 22;

  int comp = strcmp(book[0].title, "");
  if(comp == 0) {
    strcpy(book[0].title, title);
    strcpy(book[0].author, author);
    memset(book[bn].borrowerID, 0, sizeof(book[bn].borrowerID));

    bn += 1;

    return 0;
  }

  strcpy(book[bn].title, title);
  strcpy(book[bn].author, author);
  memset(book[bn].borrowerID, 0, sizeof(book[bn].borrowerID));

  bn += 1;

  collection btemp;

  for(int i=0; i<bn -1; ++i) {
    for(int j=i+1; j<bn; ++j) {
      if(strcmp(book[i].title, book[j].title) > 0){
        btemp = book[i];
        book[i] = book[j];
        book[j] = btemp;
      }
    }
  }

  return 0;
}


//Function to remove a selected book from the book variable. This will rearrange
//the structs to fill gaps and memset/NULL the final book.
int remove_book(collection *book, int select) {

  collection btemp;

  for(int i=select, j=select+1; i<bn; i++,j++) {
    book[i] = book[j];
  }

  memset(book[bn].title, 0, sizeof(book[bn].title));
  memset(book[bn].author, 0, sizeof(book[bn].author));
  memset(book[bn].borrowerID, 0, sizeof(book[bn].borrowerID));

  bn -= 1;

  nav = 22;

  return 1;
}

//Function to borrow a book and set the books borrow ID to that of the students.
int borrow_book(collection *book, user *student, int select, int stu) {
  strcpy(book[select].borrowerID, student[stu].ID);

  nav = 23;

  return 1;
}

//Function for student to return selected book.
int return_book(collection *book, int select) {
  memset(book[select].borrowerID, 0, sizeof(book[select].borrowerID));

  nav = 23;

  return 1;
}

//List all borrowed books of a specific student in a table.
void list_all_borrowed(collection *book, user *student, int stu) {
  int titlespace = max_title-5, authorspace = max_author-6;
  printf("\n\n");
  printf("   TITLE");
  for(int i=0; i<=titlespace; i++) {
    printf(" ");
  }
  printf("|");
  printf("AUTHOR");
  for(int i=0; i<=authorspace; i++) {
    printf(" ");
  }

  for(int i=0; i<=bn; i++){
    int pch = strcmp(book[i].borrowerID, student[stu].ID);

    if(pch == 0) {
      int j = i;

      int titlespace = max_title-strlen(book[j].title), authorspace = max_author-strlen(book[j].author);
      printf("\n%i. %s", j, book[j].title);
      for(int i=0; i<=titlespace; i++) {
        printf(" ");
      }
      printf("|");
      printf("%s", book[j].author);
      for(int i=0; i<=authorspace; i++) {
        printf(" ");
      }
    }
  }
  printf("\n\n");

  nav = 23;
}

//List all available books in a table.
void list_all_books(collection *book) {
  int titlespace = max_title-5, authorspace = max_author-6;
  printf("\n");
  printf("   TITLE");
  for(int i=0; i<=titlespace; i++) {
    printf(" ");
  }
  printf("|");
  printf("AUTHOR");
  for(int i=0; i<=authorspace; i++) {
    printf(" ");
  }

  for(int j=0; j < bn; j++) {
    if(book[j].borrowerID[0] == NULL) {
      int titlespace = max_title-strlen(book[j].title), authorspace = max_author-strlen(book[j].author);

      printf("\n%i. %s", j, book[j].title);
      for(int i=0; i<=titlespace; i++) {
        printf(" ");
      }
      printf("|");
      printf("%s", book[j].author);
      for(int i=0; i<=authorspace; i++) {
        printf(" ");
      }
    }
  }
  printf("\n\n");

  if(ad == 1) {
    nav = 22;
  }

  if(stu != (-1)) {
    nav = 23;
  }
}


//Function to search book author and title for a specific search string.
//Result listed in table format.
void search_books(collection *book, char search[]) {
  int titlespace = max_title-5, authorspace = max_author-6;
  printf("\n\n");
  printf("   TITLE");
  for(int i=0; i<=titlespace; i++) {
    printf(" ");
  }
  printf("|");
  printf("AUTHOR");
  for(int i=0; i<=authorspace; i++) {
    printf(" ");
  }

  for(int i=0; i<=bn; i++){
    if(book[i].borrowerID[0] == NULL) {
      char pch = strstr(book[i].title, search);
      char pch2 = strstr(book[i].author, search);

      if(pch || pch2) {
        int j = i;

        int titlespace = max_title-strlen(book[j].title), authorspace = max_author-strlen(book[j].author);
        printf("\n%i. %s", j, book[j].title);
        for(int i=0; i<=titlespace; i++) {
          printf(" ");
        }
        printf("|");
        printf("%s", book[j].author);
        for(int i=0; i<=authorspace; i++) {
          printf(" ");
        }
      }
    }
  }
  printf("\n\n");

  if(ad == 1) {
    nav = 22;
  }

  if(stu != (-1)) {
    nav = 23;
  }
}

//Function to list all borrowed books with their borrowers ID. Results listed
//in table format.
void list_all_borrowedd(collection *book) {

  int titlespace = max_title-5, authorspace = max_author-6, idspace = max_ID-10;
  printf("\n");
  printf("   TITLE");
  for(int i=0; i<=titlespace; i++) {
    printf(" ");
  }
  printf("|");
  printf("AUTHOR");
  for(int i=0; i<=authorspace; i++) {
    printf(" ");
  }
  printf("|");
  printf("STUDENT ID");
  for(int i=0; i<=idspace; i++) {
    printf(" ");
  }

  for(int j=0; j < bn; j++) {
    if(book[j].borrowerID[0] != NULL) {
      int titlespace = max_title-strlen(book[j].title), authorspace = max_author-strlen(book[j].author);
      int idspace = max_ID-strlen(book[j].borrowerID);

      printf("\n%i. %s", j, book[j].title);
      for(int i=0; i<=titlespace; i++) {
        printf(" ");
      }
      printf("|");
      printf("%s", book[j].author);
      for(int i=0; i<=authorspace; i++) {
        printf(" ");
      }
      printf("|");
      printf("%s", book[j].borrowerID);
      for(int i=0; i<=idspace; i++) {
        printf(" ");
      }
    }
  }
  printf("\n\n");

  nav = 22;
}

//Function to search all borrowed books and their borrowers ID.
//Results listed in table format
void search_borrowed(collection *book, char search[]) {
  int titlespace = max_title-5, authorspace = max_author-6, idspace = max_ID - 10;

  printf("\n\n");
  printf("   TITLE");
  for(int i=0; i<=titlespace; i++) {
    printf(" ");
  }
  printf("|");
  printf("AUTHOR");
  for(int i=0; i<=authorspace; i++) {
    printf(" ");
  }
  printf("|");
  printf("STUDENT ID");
  for(int i=0; i<=idspace; i++) {
    printf(" ");
  }

  for(int i=0; i<=bn; i++){
    if(book[i].borrowerID[0] != NULL) {
      char pch = strstr(book[i].title, search);
      char pch2 = strstr(book[i].author, search);
      char pch3 = strstr(book[i].borrowerID, search);

      if(pch || pch2 || pch3) {
        int j = i;

        int titlespace = max_title-strlen(book[j].title), authorspace = max_author-strlen(book[j].author);
        int idspace = max_ID-strlen(book[j].borrowerID);

        printf("\n%i. %s", j, book[j].title);
        for(int i=0; i<=titlespace; i++) {
          printf(" ");
        }
        printf("|");
        printf("%s", book[j].author);
        for(int i=0; i<=authorspace; i++) {
          printf(" ");
        }
        printf("|");
        printf("%s", book[j].borrowerID);
        for(int i=0; i<=idspace; i++) {
          printf(" ");
        }
      }
    }
  }
  printf("\n\n");

  nav = 22;
}
