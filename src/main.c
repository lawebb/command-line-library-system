#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
//
// #include "librarian.h"
// #include "student.h"
// #include "book.h"
// #include "login.h"
// #include "userInterface.h"
// #include "inputChecker.h"
#include "structs.h"

//Module to handle the main function of the software and bring everything together.

#define max_name 30
#define max_studentID 10
#define max_email 50
#define max_password 20
#define max_title 30
#define max_author 30

char name[max_name], email[max_email], password[max_password];
char ID[max_studentID], title[max_title], author[max_author];
char trial[100], search[30];

int n = 0, bn = 0, bmaxSize = 0,  maxSize = 0, nav = 0, select, ad, stu, ans;

admin librarian;


//Main Function to handle everything.
int main() {
  //Creating the initial librarian account.
  printf("\nPlease create your librarian account.\n");
  request_librarian_info();

  //Dynamically allocating student and book memory.
  user *student = malloc(sizeof(user) * maxSize);

  collection *book = malloc(sizeof(collection) * bmaxSize);

  //Setting a loop to run while the user doesnt select 'exit'.
  while (nav != 100) {
    //If nav is 0, open the initial interface.
    if(nav == 0) {
      opening_interface();
    }

    //If nav is set to 11, attempt to log in.
    if(nav == 11) {
      init_login(student, librarian);
    }

    //If nav is set to 12, check to see if student data structure has
    //enough dynamic memory. If not, allocate more and then register a new
    //student
    if(nav == 12) {
      if(n == maxSize) {
        if(maxSize == 0) {
          maxSize = 2;
          student = (user*) realloc(student, sizeof(user) * maxSize);
        }
        else{
          maxSize *= 2;
          student = (user*) realloc(student, sizeof(user) * maxSize);
        }
      }

      student_register_application(student);

      printf("\nYour account has been created!\n");
    }

    //If nav is set to 13, simply list all books.
    if(nav == 13) {
      list_all_books(book);
    }

    //If nav is set to 14, present the search engine.
    if(nav == 14) {
      scanf("%c", &trial);
      printf("\nPlease enter your search: ");

      scanf("%[^\n]%*c", search);
      search_books(book, search);
    }

    //If nav is set to 15, check to see if book variable has enough dynamic
    //memory. If not, allocate more and add a new book.
    if(nav == 15) {
      if(bn == bmaxSize) {
        if(bmaxSize == 0) {
          bmaxSize = 2;
          book = (collection*) realloc(book, sizeof(collection) * bmaxSize);
        }
        else{
          bmaxSize *= 2;
          book = (collection*) realloc(book, sizeof(collection) * bmaxSize);
        }
      }

      request_book_details(book);

      printf("Book has been added.\n");
    }

    //If nav is set to 16, list all books and ask the user which book they want
    // to remove. If the book variable is large enough after removal, deallocate
    //some dynamic memory.
    if(nav == 16) {
      list_all_books(book);

      printf("\nEnter the number of the book you wish to remove: ");
      scanf("%i", &select);

      if(select < 0 || select >= bn) {
        printf("\nThis book doesn't exist.\n");

        nav = 22;
      }

      else {
        remove_book(book, select);

        printf("Book removed.\n");

        if(bn == (bmaxSize / 4)) {
          bmaxSize /= 2;
          book = (collection*) realloc(book, sizeof(collection) * bmaxSize);
        }
      }
    }


    //If nav is set to 17, list all students and ask the user which student they
    //wish to remove. If student variable is large enough after removal,
    //deallocate some dynamic memory.
    if(nav == 17) {
      list_all_students(student);

      printf("\nEnter the number of the student you wish to remove: ");
      scanf("%i", &select);

      if(select < 0 || select >= n) {
        printf("\nThis student doesn't exist.\n");

        nav = 22;
      }

      else {
        remove_student(student, select);

        printf("Student removed.");

        if(n == (maxSize / 4)) {
          maxSize /= 2;
          student = (user*) realloc(student, sizeof(user) * maxSize);
        }
      }
    }

    //If nav is set to 18, list all students in table.
    if(nav == 18) {
      list_all_students(student);
    }

    //If nav is set to 19, open the search engine and get user input to search.
    if(nav == 19) {
      scanf("%c", &trial);
      printf("\nPlease enter your search: ");
      scanf("%[^\n]%*c", search);

      search_students(student, search);
    }

    //If nav is set to 20, list all books and ask the user which book they wish
    //to borrow.
    if(nav == 20) {
      list_all_books(book);

      printf("\nEnter the number of the book you wish to borrow: ");
      scanf("%i", &select);

      if(select < 0 || select >= bn) {
        printf("\nThis book doesn't exist.\n");

        nav = 22;
      }

      else {
        borrow_book(book, student, select, stu);

        printf("\nBook borrowed.\n");
      }
    }

    //If nav is set to 21, list all books that student has borrowed and
    //ask them which one they want to return.
    if(nav == 21) {
      list_all_borrowed(book, student, stu);

      printf("\nEnter the number of the book you wish to return: ");
      scanf("%i", &select);

      if(select < 0 || select >= bn) {
        printf("\nThis book doesn't exist.\n");

        nav = 22;
      }

      else {
        return_book(book, select);

        printf("\nBook returned.\n");
      }
    }

    //If nav is set to 22, open the librarians interface.
    if(nav == 22) {
      librarian_interface_one();
    }

    //If nav is set to 23, open the students interface.
    if(nav == 23) {
      student_interface_one();
    }

    //If nav is set to 24, list all books borrowed by all students
    if(nav == 24) {
      list_all_borrowedd(book);
    }

    //If nav is set to 25, open search engine to allow user to input search
    //sample to search borrowed books of all students.
    if(nav == 25) {
      scanf("%c", &trial);
      printf("\nPlease enter your search: ");

      scanf("%[^\n]%*c", search);
      search_borrowed(book, search);
    }
  }

  //Free the dynamically allocated memory and exit the program.
  free(book);
  free(student);

  printf("\n\nThankyou for using Libration!\n\n");

  exit(0);

  return 0;
}
