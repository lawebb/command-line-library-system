#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>

#include "structs.h"

//Module to handle the majority of user input to check validity.

#define max_name 30
#define max_ID 10
#define max_course 20
#define max_email 50
#define max_password 20
#define max_contact_number 11
#define max_title 30
#define max_author 30
#define max_date_published 10
#define max_subject 20

extern char name[max_name], email[max_email], password[max_password];
extern char ID[max_ID], title[max_title], author[max_author];
extern char datePublished[max_date_published], trial[];

int i;
bool valid;

//Function to NULL trial variable for reuse.
int null_trial() {
  for(int i=0; i < 99; i++) {
    trial[i] = NULL;
  }

  return 1;
}

//Function to request, scan and then check the user input for name. Uses
//name check function below to check validity and pass back.
void ask_name_input() {
  null_trial();

  printf("\nPlease enter your name: ");
  scanf("%[^\n]%*c", trial);

  check_name_input(trial);

  if(!valid) {
    ask_name_input();
  }
  else if(valid) {
    trial[0] = toupper(trial[0]);

    for(int i = 1; trial[i]; i++) {
      trial[i] = tolower(trial[i]);
    }
    strcpy(name, trial);
  }
}

int check_name_input(char name[]) {
  int count = 0;

  for(i=0; i<=31; i++) {
    if(name[i] != NULL) {
      if(isalpha(name[i]) || name[i] == ' ') {

        count += 1;

        valid = true;
      }
      else {
        printf("\nThe name you entered contained non-character input. Please try again.\n");

        valid = false;

        return 1;
      }
    }
  }

  if(count > 30) {
    printf("\nThe name you entered is too long. Please try again.");

    valid = 0;

    return false;
  }
  else{
    valid = true;
  }
  return 0;
}


//Function to request, scan and then check the user input for email. Uses
//email check function below to check validity and pass back.
void ask_email_input() {
  null_trial();

  printf("\nPlease enter your Email: ");
  scanf("%[^\n]%*c", trial);

  check_email_input(trial);

  if(!valid) {
    ask_email_input();
  }
  else if(valid) {
    strcpy(email, trial);
  }
}

int check_email_input(char email[]) {
  int count = 0, atCount = 0;

  for(i=0; i<=51; i++) {
    if(email[i] != NULL) {
      count++;

      valid = 1;

      if(email[i] == '@') {
        atCount++;
      }
    }
  }

  if(count > 50) {
    printf("\nThe email you entered is too long. Please try again.");

    valid = 0;

    return 1;
  }

  if(atCount != 1) {
    printf("\nThere must be and can only be 1 '@' within your email.");

    valid = 0;

    return 1;
  }
  return 0;
}


//Function to request, scan and then check the user input for password. Uses
//password check function below to check validity and pass back.
void ask_password_input() {
  null_trial();

  printf("\nPlease enter your password: ");
  scanf("%[^\n]%*c", trial);

  check_password_input(trial);

  if(!valid) {
    ask_password_input();
  }
  else if(valid) {
    strcpy(password, trial);
  }
}

int check_password_input(char password[]) {
  int count = 0,ucount = 0, lcount = 0, ncount = 0;
  for(i=0; i<=21; i++) {
    if(password[i] != NULL) {
      count++;
    }
    if(isdigit(password[i])) {
      ncount++;
    }
    if(isupper(password[i])) {
      ucount++;
    }
    if(islower(password[i])) {
      lcount++;
    }
  }

  valid = 1;

  if(count > 20) {
    printf("\nThe password you entered is too long (must be between 8 and 20 characters). Please try again.");

    valid = 0;

    return 1;
  }
  if(count < 8) {
    printf("\nThe password you entered is too short (must be between 8 and 20 characters). Please try again.");

    valid = 0;

    return 1;
  }
  else if(ncount < 1 || ucount < 1 || lcount < 1) {
    printf("\nYour password must contain at least 1 upper case character, 1 lower case character and 1 digit.");

    valid = 0;

    return 1;
  }
  return 0;
}


//Function to request, scan and then check the user input for ID. Uses
//ID check function below to check validity and pass back.
void ask_ID_input() {
  null_trial();

  printf("\nPlease enter your student ID: ");
  scanf("%[^\n]%*c", trial);

  check_ID_input(trial);

  if(!valid) {
    ask_ID_input();
  }
  else if(valid) {
    strcpy(ID, trial);
  }
}

int check_ID_input(char ID[]) {
  int count = 0;
  for(i=0; i<=11; i++) {
    if(ID[i] != NULL) {
      count++;

      valid = 1;
    }
  }
  if(count > 10) {
    printf("\nThe ID you entered is too long. Please try again.");

    valid = 0;

    return 1;
  }
  return 0;
}


//Function to request, scan and then check the user input for title. Uses
//title check function below to check validity and pass back.
void ask_title_input() {
  null_trial();

  printf("\nPlease enter the book title: ");
  scanf("%[^\n]%*c", trial);

  check_title_input(trial);

  if(!valid) {
    ask_title_input();
  }
  else if(valid) {
    strcpy(title, trial);
  }
}

int check_title_input(char title[]) {
  int count = 0;
  for(i=0; i<=31; i++) {
    if(title[i] != NULL) {
      count++;

      valid = 1;
    }
  }
  if(count > 30) {
    printf("\nThe title you entered is too long. Please try again.");

    valid = 0;

    return 1;
  }
  return 0;
}

//Function to request, scan and then check the user input for author. Uses
//author check function below to check validity and pass back.
void ask_author_input() {
  null_trial();

  printf("\nPlease enter the book author name: ");
  scanf("%[^\n]%*c", trial);

  check_author_input(trial);

  if(!valid) {
    ask_author_input();
  }
  else if(valid) {
    strcpy(author, trial);
  }
}

int check_author_input(char author[]) {
  int count = 0;

  for(i=0; i<=31; i++) {
    if(author[i] != NULL) {
      if(isalpha(author[i]) || author[i] == ' ') {

        count += 1;

        valid = true;
      }
      else {
        printf("\nThe name you entered contained non-character input. Please try again.\n");

        valid = false;

        return 1;
      }
    }
  }
  return 0;
}
