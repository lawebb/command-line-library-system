#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
//
// #include "librarian.h"
// #include "student.h"
// #include "book.h"
// #include "login.h"
// #include "userInterface.h"
// #include "inputChecker.h"
#include "structs.h"
#include "unity.h"

//Module to handle the Unity tests.

#define max_name 30
#define max_studentID 10
#define max_email 50
#define max_password 20
#define max_title 30
#define max_author 30

char name[max_name], email[max_email], password[max_password];
char ID[max_studentID], title[max_title], author[max_author];
char trial[100], search[30];

int n = 0, bn = 0, bmaxSize = 3,  maxSize = 3, nav = 0, select, ad, stu, ans;

admin librarian;

user *student;

collection *book;


//Function to test the creation of the librarian.
void test_librarian(void) {

  create_librarian("LibEmail@school.com", "Librarian1");

  TEST_ASSERT_EQUAL_STRING("LibEmail@school.com", librarian.email);
  TEST_ASSERT_EQUAL_STRING("Librarian1", librarian.password);
}


//Function to test the adding and removing of students.
void test_student(void) {
  user *student = malloc(sizeof(user) * maxSize);

  add_student(student, "Steve", "SW66", "steve@school.com", "SWStudent3");
  add_student(student, "Liam", "LW98", "liam@school.com", "LWStudent1");
  add_student(student, "Rachel", "RP77", "rachel@school.com", "RPStudent2");

  TEST_ASSERT_EQUAL_STRING("Liam", student[0].name);
  TEST_ASSERT_EQUAL_STRING("LW98", student[0].ID);
  TEST_ASSERT_EQUAL_STRING("liam@school.com", student[0].email);
  TEST_ASSERT_EQUAL_STRING("LWStudent1", student[0].password);

  TEST_ASSERT_EQUAL_STRING("Rachel", student[1].name);
  TEST_ASSERT_EQUAL_STRING("RP77", student[1].ID);
  TEST_ASSERT_EQUAL_STRING("rachel@school.com", student[1].email);
  TEST_ASSERT_EQUAL_STRING("RPStudent2", student[1].password);

  TEST_ASSERT_EQUAL_STRING("Steve", student[2].name);
  TEST_ASSERT_EQUAL_STRING("SW66", student[2].ID);
  TEST_ASSERT_EQUAL_STRING("steve@school.com", student[2].email);
  TEST_ASSERT_EQUAL_STRING("SWStudent3", student[2].password);

  remove_student(student, 1);

  TEST_ASSERT_EQUAL_STRING("Liam", student[0].name);
  TEST_ASSERT_EQUAL_STRING("LW98", student[0].ID);
  TEST_ASSERT_EQUAL_STRING("liam@school.com", student[0].email);
  TEST_ASSERT_EQUAL_STRING("LWStudent1", student[0].password);

  TEST_ASSERT_EQUAL_STRING("Steve", student[1].name);
  TEST_ASSERT_EQUAL_STRING("SW66", student[1].ID);
  TEST_ASSERT_EQUAL_STRING("steve@school.com", student[1].email);
  TEST_ASSERT_EQUAL_STRING("SWStudent3", student[1].password);

  TEST_ASSERT_EQUAL_STRING("", student[2].name);
  TEST_ASSERT_EQUAL_STRING("", student[2].ID);
  TEST_ASSERT_EQUAL_STRING("", student[2].email);
  TEST_ASSERT_EQUAL_STRING("", student[2].password);
}


//Function to test the adding, borrowing, returning and removing of books.
void test_book(void) {
  collection *book = malloc(sizeof(collection) * bmaxSize);

  add_book(book, "The Holiday", "Mark Sterling");
  add_book(book, "Devil", "Jeremy Smithers");
  add_book(book, "Last Ditch", "Terry Wallace");

  TEST_ASSERT_EQUAL_STRING("Devil", book[0].title);
  TEST_ASSERT_EQUAL_STRING("Jeremy Smithers", book[0].author);

  TEST_ASSERT_EQUAL_STRING("Last Ditch", book[1].title);
  TEST_ASSERT_EQUAL_STRING("Terry Wallace", book[1].author);

  TEST_ASSERT_EQUAL_STRING("The Holiday", book[2].title);
  TEST_ASSERT_EQUAL_STRING("Mark Sterling", book[2].author);

  user *student = malloc(sizeof(user) * maxSize);

  add_student(student, "Liam", "LW98", "liam@school.com", "LWStudent1");

  TEST_ASSERT_EQUAL_STRING("", book[2].borrowerID);

  borrow_book(book, student, 2, 0);

  TEST_ASSERT_EQUAL_STRING(student[0].ID, book[2].borrowerID);

  return_book(book, 2);

  TEST_ASSERT_EQUAL_STRING("", book[2].borrowerID);

  remove_book(book, 1);

  TEST_ASSERT_EQUAL_STRING("Devil", book[0].title);
  TEST_ASSERT_EQUAL_STRING("Jeremy Smithers", book[0].author);

  TEST_ASSERT_EQUAL_STRING("The Holiday", book[1].title);
  TEST_ASSERT_EQUAL_STRING("Mark Sterling", book[1].author);

  TEST_ASSERT_EQUAL_STRING("", book[2].title);
  TEST_ASSERT_EQUAL_STRING("", book[2].author);
}


//Main function to run the tests.
int main(void) {

  UNITY_BEGIN();

  RUN_TEST(test_librarian);
  RUN_TEST(test_student);
  RUN_TEST(test_book);

  return UNITY_END();
}
