#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>

#include "structs.h"
#include "inputChecker.h"

//Module to handle librarian data.

#define max_email 50
#define max_password 20

extern char email[], password[], trial[];

admin librarian;

//Function to ask for and check input for librarian.
void request_librarian_info() {

  ask_email_input();

  ask_password_input();

  create_librarian(email, password);

  printf("\nYour account has been created!\n");
}


//Function to create/add data to the librarian struct
void create_librarian(char email[], char password[]) {
  strcpy(librarian.email, email);
  strcpy(librarian.password, password);
}
