# Command Line Library System

My implementation of a command line library system that can be used by both
librarians and students to manage books and students in the system.


# Why I made this programm

I made this programm as a project at the end of my first year of university.
It was great to practice with linked lists, pointers and dynamically
allocated memory.


# Frameworks used

    * CMake

# Features

Students can:

    * Log in or register on the system.
    * Search, list, borrow and retrun books.
    
Librarians can:

    * Log in or register on the system.
    * Search, list and remove students.
    * Add, remove, search and list books.
    
    
# How to run on Linux

Set up directories as shown in my repository and cd into the root directory,
within the command line. You can then either choose to run the tests, or the 
main programm.

    * $ cmake .
    * $ make
    * $ ./tests
    * $ ./main
    

# Working Examples

Librarian Registration and opening screen:

![UI_1](/images/opening-screen.png)


Registering students:

![register](/images/registration.png)


Student user interface:

![SUI](/images/student-UI.png)


Librarian user interface:

![LUI](/images/librarian-UI.png)


List/search for students:

![search](/images/student-search.png)


Remove students:

![remove](/images/remove-student.png)


# Reports and Specification

System Spec:
![system-spec](/images/specification.png)

[Link 1: Planning Report](https://gitlab.com/lawebb/command-line-library-system/-/blob/master/reports/Planning%20Report.pdf)

Planning Specification:
![planning-spec](/images/reportspec.png)

[Link 2: Final Report](https://gitlab.com/lawebb/command-line-library-system/-/blob/master/reports/Final%20Report.pdf)

Planning Specification:
![finalreport-spec](/images/freportspec.png)


